import React, { Component } from "react";
import { Form, Button,Image } from "react-bootstrap";

class MyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailErr:"",
      passwordErr:"",
      isSignin: false,
      image: null,
      imageURL: null,
        gender:"female"
    };
  }

  onHandleText = (e) => {
    console.log("e :", e.target);

    this.setState({
        [e.target.name]: e.target.value
    },
    ()=>{
        console.log(this.setState)

        if(e.target.name==="email"){
            //apple1212@#gmail.com

            let pattern=/^\S+@\S+\.[a-z]{3}$/g;

            let result = pattern.test(this.state.email.trim())
        
            if(result){
                this.setState({
                    emailErr:""
                });

            }
            else if(this.state.email===""){
                this.setState({
                    emailErr:"Email cannot be empty!"
                });
            }
            else{
                this.setState({
                    emailErr:"Email is invalid!"
                });
            }

        }
        if(e.target.name==="password"){
            //apple1212@#gmail.com

            let pattern2=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\W])[a-zA-Z0-9_\W]{8,}$/g;

            let result2 = pattern2.test(this.state.name)
        
            if(result2){
                this.setState({
                    passwordErr:""
                });
            }
            else if(this.state.password===""){
                this.setState({
                    passwordErr:"password cannot be empty!"
                });
            }
            else{
                this.setState({
                    passwordErr:"password is invalid!"
                });
            }

        }
        
    });
};

validateBtn=()=>{
    if(this.state.emailErr===""&&
     this.state.passwordErr==="" &&
      this.state.email !==""&& 
      this.state.password !==""){
        return false
    }
    else{
        return true
    }
};

onUploadFile=(e) =>{
    console.log("Image: ",e.target.files[0])
    this.setState({
        imageURL: URL.createObjectURL(e.target.files[0])
    });

};

  render() {
    return (
      <Form>
        <label htmlfor="myfile">
          <Image width="200px" height="200px" src="mdb.jpg" roundedCircle />
        </label>

        <input
          onChange={this.onUploadFile}
          style={{ display: "none" }}
          id="myfile"
          type="file"
        />
        <h1>Create Account</h1>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control type="email" placeholder="username" />
        </Form.Group>

        <Form.Label>Gender</Form.Label>
        <Form.Check
          custom
          inline
          label="Male"
          tyle="radio"
          id="male"
          name="gender"
          defaultChecked={this.state.gender === "male" ? true : false}
        />
        <Form.Check
          custom
          inline
          label="Female"
          tyle="radio"
          id="female"
          name="gender"
          defaultChecked={this.state.gender === "female" ? true : false}
        />
        <br />

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            name="email"
            onChange={this.onhandleText}
            type="email"
            placeholder="Email"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.emailErr}</p>
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            onChange={this.onHandleText}
            type="email"
            placeholder="Password"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.passwordErr}</p>
          </Form.Text>
        </Form.Group>

        <Button disable={this.validateBtn()} variant="primary" type="button">
          Save
        </Button>
        {/* <Button onClick={this.onTest}>Test</Button> */}
      </Form>
    );
  }
}
 
export default MyForm;