import React from "react";
import { Table,Button } from "react-bootstrap";

export default function MyTable ({user, onClear}) {
  console.log("Props on func:", user);

  let temp=user.filter(item=>{
      return item.qty>0
  })

  return (
    <>
      <Button onClick={onClear} variant="Danger">
        Delete
      </Button>
      <h2>{temp.length} Table Accounts</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Username</th>
            <th>Email</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>
          {temp.map((item, index) => (
            <tr>
              <td></td>
              <td>{index + 1}</td>
              <td>{item.Username}</td>
              <td>{item.Email}</td>
              <td>{item.Gender}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
