import React from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import MyTable from "./components/MyTable";
import MyCard from "./components/MyCard";
import { Container, Row } from "react-bootstrap";


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [
        {
          id: 1,
          name: "Marvin",
          email: "mavinsao11@gmail.com",
          gender: "male",
        },
        {
          id: 2,
          name: "Leangseng",
          email: "leangseng@gmail.com",
          gender: "male",
        },
      ],
    };
  }

  onSave = (index) => {
    console.log("index:", index);
    let temp = [...this.state.user];
    temp[index].qty++;
    temp[index].total = temp[index].qty * temp[index].price;

    this.setState({
      user: temp,
    });
  };

  onDelete = (index) => {
    console.log("index:", index);
    let temp = [...this.state.user];
    temp[index].qty--;
    temp[index].total = temp[index].qty * temp[index].price;

    this.setState({
      user: temp,
    });
  };

  onClear = () => {
    console.log("Clearing...");
    let user = [...this.state.user];
    // eslint-disable-next-line array-callback-return
    user.map((item) => {
      item.qty = 0;
      item.total = 0;
    });
    this.setState({
      user,
    });
  };

  render() {
    return (
      <Container>
        <Row>
          <MyCard
            user={this.state.user}
            onSave={this.onSave}
            onDelete={this.onDelete}
          />
        </Row>
        <Row>
          <MyTable onClear={this.onClear} user={this.state.user} />
        </Row>
      </Container>
    );
  }
}


